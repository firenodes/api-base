# @flowtr/api-base

A basic authentication api that consists of a mongodb database connection, tinyhttp web server, and typescript.
It uses the user schema from the common library as a base.
We am going to use this as a base for most of our typescript apis.

## Usage

```typescript
import { createApp } from "@flowtr/api-base";

const app = createApp();
```
