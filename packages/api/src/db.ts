import { Config } from "./config";

import monk from "monk";

export const createDatabase = async (config: Config) => {
    return monk(config.dbUri);
};
