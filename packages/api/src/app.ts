import { Config } from "./config";
import { App, NextFunction, Request, Response } from "@tinyhttp/app";
import bcrypt from "bcrypt";
import {
    BaseUserModel,
    httpResponse,
    userSchema,
} from "@flowtr/api-base-common";
import { createDatabase } from "./db";
import jwt from "jsonwebtoken";
import { json } from "milliparsec";

import { userLoginSchema } from "./schemas/user";

export interface AuthenticatedRequest<U extends BaseUserModel> extends Request {
    user?: U;
}

export const createApp = async <
    C extends Config = Config,
    U extends BaseUserModel = BaseUserModel
>(
    configFn: () => Promise<C>,
    createFn: (config: C) => Promise<App> = async () => new App(),
    createUser: (
        base: BaseUserModel,
        req: Request,
        res: Response,
        next: NextFunction
    ) => Promise<U> = async (base: BaseUserModel) => base as U
) => {
    const config = await configFn();
    const app = await createFn(config);
    app.use(json());
    // Initialize the database connection
    const db = await createDatabase(config);
    const users = db.get<BaseUserModel>("users");
    users.createIndex("id");

    const verifyToken = async (
        req: AuthenticatedRequest<U>,
        res: Response,
        next: NextFunction
    ) => {
        const authHeader = req.headers["authorization"];
        const token = authHeader && authHeader.split(" ")[1];
        if (token == null)
            return res.status(401).json(
                await httpResponse.parseAsync({
                    message: `Not Authenticated`,
                })
            );

        try {
            const user: U = jwt.verify(token, config.jwtSecret) as U;
            req.user = user;
            next();
        } catch (err) {
            if (err)
                return res.status(401).json(
                    await httpResponse.parseAsync({
                        message: `Invalid Token`,
                    })
                );
        }
    };

    const auth = app.route("/auth");
    auth.post("/register", async (req, res, next) => {
        try {
            const user = await userSchema.parseAsync(
                await createUser(req.body, req, res, next)
            );
            const salt = await bcrypt.genSalt(10);
            // Hash the raw password
            const hashedPassword = await bcrypt.hash(user.password, salt);
            user.password = hashedPassword;
            users.insert(user as U);
            return res.status(201).json(
                await httpResponse.parseAsync({
                    message: `Succesfully created user: ${user.username}`,
                    result: await users.findOne(),
                })
            );
        } catch (err) {
            return next(err);
        }
    });
    auth.get("/", async (req, res, next) => {
        try {
            const data = await userLoginSchema.parseAsync(req.body);
            const user = await users.findOne({ username: data.username });
            if (!(await bcrypt.compare(data.password, user.password)))
                return res.status(400).json(
                    await httpResponse.parseAsync({
                        message: `Invalid Credentials`,
                    })
                );
            const token = jwt.sign(user, config.jwtSecret);
            return res.status(200).json(
                await httpResponse.parseAsync({
                    message: `Succesfully created user: ${user.username}`,
                    result: { accessToken: token },
                })
            );
        } catch (err) {
            next(err);
        }
    });
    auth.get(
        "/profile",
        verifyToken,
        async (req: AuthenticatedRequest<U>, res) =>
            res.status(200).json(
                await httpResponse.parseAsync({
                    message: "Succesfully retrieved profile",
                    result: await users.findOne({
                        username: req.user.username,
                    }),
                })
            )
    );
    return { auth, app, config, users, db, verifyToken };
};
