import * as z from "zod";
import { nanoid } from "nanoid";

export const configSchema = z.object({
    dbUri: z.string().url(),
    jwtSecret: z
        .string()
        .min(5)
        .default(() => nanoid(20)),
});

export type Config = z.infer<typeof configSchema>;
