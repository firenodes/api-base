/**
 * Typeorm insert result
 */
export interface InsertResult {
    /**
     * Contains inserted entity id.
     * Has entity-like structure (not just column database name and values).
     */
    identifiers: Record<string, unknown>[];
    /**
     * Generated values returned by a database.
     * Has entity-like structure (not just column database name and values).
     */
    generatedMaps: Record<string, unknown>[];
    /**
     * Raw SQL result returned by executed query.
     */
    raw: unknown;
}
