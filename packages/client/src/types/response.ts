export interface ApiResponse<T = unknown> {
    error?: Error | string;
    /**
     * Usually can either be a blog post, user or insert result.
     */
    data?: T;
}
