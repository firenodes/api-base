import { BaseEntity } from "./base";

export interface User extends BaseEntity {
    username: string;
    name: string;
    roles: string[];
    avatar?: string;
}
