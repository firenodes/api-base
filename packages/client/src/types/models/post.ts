import { BaseEntity } from "./base";
import { User } from "./user";

export interface BlogPost extends BaseEntity {
    title: string;
    description: string;
    content: string;
    author: User;
    category: string;
}
