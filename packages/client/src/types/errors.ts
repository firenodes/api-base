export enum Errors {
    AUTHENTICATION = "Authentication error encountered - double check your credentials and make sure you are logged in.",
    NO_RESPONSE = "No response from backend.",
}
