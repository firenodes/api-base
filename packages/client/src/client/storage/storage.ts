export interface Storage<S = unknown> {
    set(data: Array<[keyof S & string, never | undefined | null]>): void;
    get<T>(key: keyof S & string): T | undefined;
}
