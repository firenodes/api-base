import axios, { AxiosResponse } from "axios";
import { ApiResponse, Errors, BlogPost, User, InsertResult } from "../types";
import { Storage } from "./storage/storage";

export interface ClientResponse<T = unknown> extends ApiResponse<T> {
    response?: AxiosResponse<T>;
}

/**
 * Throw Out Error API Client
 * Made using axios
 */

export interface ApiStorage {
    user: User;
    token?: string;
}

export class ApiService {
    storage: Storage<ApiStorage>;
    apiUrl: string;

    /**
     * @param storage A storage implementation for persisting the user data. An example of this is SessionStorage.
     * @param apiUrl Optional api url, defaults to the throw-out-error heroku one.
     */
    constructor(storage: Storage<ApiStorage>, apiUrl?: string) {
        this.storage = storage;
        this.apiUrl = apiUrl || "https://throw-out-error-api.herokuapp.com";
    }

    isLoggedIn(): boolean {
        return (
            this.storage.get("token") !== undefined &&
            this.storage.get("user") !== undefined
        );
    }

    hasRole(role: string): boolean {
        return this.isLoggedIn()
            ? (this.storage.get<User>("user") as User).roles.includes(role)
            : false;
    }

    async logout(): Promise<ClientResponse> {
        try {
            const response = await axios.post(
                this.apiUrl + "/auth/logout",
                { token: this.storage.get<string>("token") },
                {
                    headers: {
                        Accept: "application/json",
                    },
                },
            );
            this.storage.set([
                ["user", null],
                ["token", null],
            ]);
            return { response };
        } catch (error) {
            this.storage.set([
                ["user", null],
                ["token", null],
            ]);
            return { error };
        }
    }

    async deletePost(data: { title: string }): Promise<ClientResponse> {
        try {
            const result = await axios.post(this.apiUrl + "/blog/post", data, {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            });
            if (!result) return { error: Errors.NO_RESPONSE };
            const json = result.data;
            if (result.status === 200)
                return {
                    response: result,
                    data: json,
                };
            else if (result.status === 401)
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    data: json,
                };
            else
                return {
                    error: "Unknown error encountered.",
                    response: result,
                    data: json,
                };
        } catch (e) {
            return {
                error: e.message,
            };
        }
    }

    async createPost(data: {
        title: string;
        description: string;
        content: string;
    }): Promise<ClientResponse<InsertResult>> {
        try {
            const result = await axios.post(this.apiUrl + "/blog/post", data, {
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                },
            });
            if (!result) return { error: Errors.NO_RESPONSE };
            const json = result.data;
            if (result.status === 201)
                return {
                    response: result,
                    data: json,
                };
            else if (result.status === 401)
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    data: json,
                };
            else
                return {
                    error: json.message ?? json.error,
                    response: result,
                    data: json,
                };
        } catch (e) {
            return {
                error: e.message,
            };
        }
    }

    async getProfile(accessToken: string): Promise<ClientResponse<User>> {
        try {
            const result = await axios.get(this.apiUrl + "/auth/profile", {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    Authorization: `${accessToken}`,
                },
            });
            if (!result) return { error: Errors.NO_RESPONSE };

            const json = result.data;

            if (result.status === 200) {
                this.storage.set([["user", json.user]]);
                return {
                    response: result,
                    data: json,
                };
            } else {
                this.storage.set([
                    ["user", null],
                    ["token", null],
                ]);
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    data: json,
                };
            }
        } catch (e) {
            return {
                error: Errors.AUTHENTICATION,
            };
        }
    }

    async sendLoginRequest(
        username: string,
        password: string,
    ): Promise<ClientResponse<User>> {
        let result: AxiosResponse | undefined;
        try {
            result = await axios.post(
                this.apiUrl + "/auth",
                {
                    username: username.toLowerCase(),
                    password,
                },
                {
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                },
            );
            if (!result) return { error: Errors.NO_RESPONSE };
            const res = result.data;

            if (result.status === 200 && res.accessToken) {
                this.storage.set([["token", res.accessToken]]);
                // Get profile if logged in
                return await this.getProfile(res.accessToken);
            } else {
                this.storage.set([
                    ["user", null],
                    ["token", null],
                ]);
                return {
                    error: Errors.AUTHENTICATION,
                };
            }
        } catch (e) {
            this.storage.set([
                ["user", null],
                ["token", null],
            ]);
            return {
                error:
                    e.response.data.error ??
                    e.response.statusText ??
                    "Unknown Error",
            };
        }
    }

    async sendRegistrationRequest(data: {
        username: string;
        password: string;
        name?: string;
    }): Promise<ClientResponse> {
        try {
            const result = await axios.post(
                this.apiUrl + "/auth/register",
                { ...data, username: data.username.toLowerCase() },
                {
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                    },
                    method: "POST",
                },
            );
            const json = await result.data;
            if (result.status === 200) {
                this.storage.set([["user", json.user]]);
                return { response: result, data: json };
            } else {
                this.storage.set([["user", null]]);
                return {
                    error: Errors.AUTHENTICATION,
                    response: result,
                    data: json,
                };
            }
        } catch (e) {
            return { error: e.message };
        }
    }

    async getPosts(opts: {
        dateFilter?: string;
        category?: string;
        query?: string;
    }): Promise<ClientResponse<BlogPost[]>> {
        let response: AxiosResponse;
        try {
            const formattedUrl = `${this.apiUrl}/blog/post?date=${
                opts.dateFilter ?? ""
            }&category=${opts.category ?? ""}&search=${opts.query ?? ""}`;
            response = await axios.get(formattedUrl);
            const posts = response.data;
            return {
                response,
                data: posts.map((p: BlogPost) => ({
                    ...p,
                    createdAt: new Date(p.createdAt),
                })),
            };
        } catch (e) {
            return {
                error: e.message,
                response,
            };
        }
    }

    async getSinglePost(id: string): Promise<ClientResponse<BlogPost>> {
        let response: AxiosResponse;
        try {
            response = await axios.get(`${this.apiUrl}/blog/post/${id}`);

            const post = {
                ...response.data,
                createdAt: new Date(response.data.createdAt),
            };
            return {
                response,
                data: post,
            };
        } catch (e) {
            return {
                error: e.message,
                response,
            };
        }
    }

    dateToString(dateObj: Date): string {
        const monthNames = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];
        const month = monthNames[dateObj.getMonth()];
        const day = String(dateObj.getDate()).padStart(2, "0");
        const year = dateObj.getFullYear();
        const output = month + "\n" + day + "," + year;
        return output;
    }
}
export * from "./storage/session";
export * from "./storage/storage";
