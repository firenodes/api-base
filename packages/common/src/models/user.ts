import * as z from "zod";
import { baseEntitySchema } from "./core";

export const userSchema = baseEntitySchema.extend({
    username: z.string().min(2),
    name: z.string().default("Unnamed User"),
    email: z.string().default("example@example.com"),
    password: z.string().min(3),
});

export type BaseUserModel = z.infer<typeof userSchema>;
6;
