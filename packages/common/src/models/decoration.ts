import * as z from "zod";

export const colorSchema = z.union([
    z.literal("gray"),
    z.literal("brown"),
    z.literal("orange"),
    z.literal("yellow"),
    z.literal("teal"),
    z.literal("blue"),
    z.literal("purple"),
    z.literal("pink"),
    z.literal("red"),
    z.literal("gray_background"),
    z.literal("brown_background"),
    z.literal("orange_background"),
    z.literal("yellow_background"),
    z.literal("teal_background"),
    z.literal("blue_background"),
    z.literal("purple_background"),
    z.literal("pink_background"),
    z.literal("red_background"),
]);

export type Color = z.infer<typeof colorSchema>;

export const boldFormat = z.tuple([z.literal("b")]);
export const italicFormat = z.tuple([z.literal("i")]);
export const strikeFormat = z.tuple([z.literal("s")]);
export const codeFormat = z.tuple([z.literal("c")]);
export const underlineFormat = z.tuple([z.literal("_")]);
export const linkFormat = z.tuple([z.literal("a"), z.string()]);
export const colorFormat = z.tuple([z.literal("h"), colorSchema]);

export const decorationSchema = z.union([
    boldFormat,
    italicFormat,
    strikeFormat,
    codeFormat,
    underlineFormat,
    linkFormat,
    colorFormat,
]);

export type Decoration = z.infer<typeof decorationSchema>;
