import * as z from "zod";
import { nanoid } from "nanoid";

export const idSchema = z.string().default(() => nanoid(50));

export const baseSchema = z.object({
    id: idSchema,
});

export const baseEntitySchema = z.object({
    id: idSchema,
    createdOn: z.date().default(() => new Date()),
    updatedOn: z.date().default(() => new Date()),
});

export type BaseModel = z.infer<typeof baseSchema>;
export type BaseEntityModel = z.infer<typeof baseEntitySchema>;
