import * as z from "zod";

export const httpResponse = z.object({
    error: z.string().optional(),
    message: z.string().optional(),
    result: z.unknown().optional(),
});
